// Klasa reprezentująca drzewo wszystkich możliwych ruchów gracza i AI
#ifndef TREE_HPP
#define TREE_HPP

#include "board.hpp"

class GameTree
{
    public:
        GameTree() {};
        ~GameTree()
        {
            int i = 0;
            while(next_boards.size() != 0)
                delete next_boards[i++];
        }
        GameTree(Board* root, unsigned int max_depth, std::string team);
        // Zwraca rozmiar wektora początkowego (poziomu 1) - funkcja do testów
        unsigned int size() { return next_boards.size(); }

    private:
        Board* target_board;
        std::vector<GameTree*> next_boards;
};

#endif // TREE_HPP