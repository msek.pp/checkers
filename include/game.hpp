// Klasa główna programu, reprezentuję samą grę:
//      - pętla główna 
//      - tworzenie klatek
//      - renderowanie okna
#ifndef GAME_HPP
#define GAME_HPP

#include "tree.hpp"

#define SCREENX 1040 // Szerokość okna
#define SCREENY 1040 // Wysokość okna
#define DEPTH 32     // Głębia

#include "board.hpp"

class Game : public sf::RenderWindow
{
    public:
        Game() {};
        ~Game() {};
        Game(sf::String title);
        void run();

    private:
        Board board;
};

#endif // GAME_HPP