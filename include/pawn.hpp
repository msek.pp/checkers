// Klasa reprezentująca pionek w warcabach - odrazu połączona z 
// graficzną reprezentacją w SFML (dziedziczenie)
#ifndef PAWN_HPP
#define PAWN_HPP

#include <iostream>
#include <SFML/Graphics.hpp>

class Pawn : public sf::Sprite
{
    public:
        // Konstruktory i destruktor
        Pawn() {};
        ~Pawn() {};
        Pawn(std::string color, const std::string filename);
        // Ustawia indeksy pionka na planszy
        void setIndex(unsigned int index1, unsigned int index2)
        {
            indices.first = index1;
            indices.second = index2;
        }
        // Ustawia koordynaty na ekranie gry
        void setCoord(int x, int y)
        {
            coords.first = x;
            coords.second = y;
        }
        // Zwraca swoje indkesy na planszy
        std::pair<unsigned int, unsigned int> retIndices() { return indices; }
        // Zwraca swoje położenie na ekranie
        std::pair<int, int> retCoords() { return coords; }
        // Zwraca symbol swojej drużyny
        std::string retTeam() { return team == "white" ? "W" : "B"; }
        // Zmienia położenie na planszy
        void move(int x, int y) { setPosition(sf::Vector2f(x, y)); }
        // Wylicza swoje położenie na podstawie indeksów
        void calcIndices();
        // Zwraca 'true' jeżeli pionek jest damką
        bool isQueen() { return queen; }
        // Zamienia pionka w królową
        void coronation(std::string queen_file)
        {
            image.loadFromFile(queen_file);
            queen = true; 
        }

    private:
        sf::Texture image;                             // Plik z obrazkiem pionka
        std::pair<unsigned int, unsigned int> indices; // Indeks pierwszy i drugi na planszy
        std::pair<int, int> coords;                    // Współrzędne "x" i "y" pionka na ekranie
        std::string team;                              // Drużyna, w której jest pionek
        bool queen = false;                            // Zmienna mówiąca czy pionek jest damką
};

#endif // PAWN_HPP