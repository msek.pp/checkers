// Klasa reprezentująca planszę do gry w warcaby - odrazu połączona z 
// graficzną reprezentacją w SFML (dziedziczenie)
#ifndef BOARD_HPP
#define BOARD_HPP

#include "pawn.hpp"

#define BOARDF "/home/higgs_001/PAMSI/PROJEKT4/include/pawns/board.png"
#define WHITEP "/home/higgs_001/PAMSI/PROJEKT4/include/pawns/white.png"
#define BLACKP "/home/higgs_001/PAMSI/PROJEKT4/include/pawns/black.png"
#define WHITEQ "/home/higgs_001/PAMSI/PROJEKT4/include/pawns/white_queen.png"
#define BLACKQ "/home/higgs_001/PAMSI/PROJEKT4/include/pawns/black_queen.png"
#define WHITETEAM "white"
#define BLACKTEAM "black"

#define board_size 8 // ROZMIAR PLANSZY
#define pawns 12     // ILOŚĆ PIONKÓW

// Wektor par indeksów, który posłuży do przechowywania pól planszy do badania 
// bić lub wielokrotnych bić
typedef std::vector<std::pair<int, int>> Areas;

class Board : public sf::Sprite
{
    public:
        Board();
        ~Board();
        // Pokazuje planszę w terminalu
        void showBoard();
        // Zwraca wartość planszy wyliczoną przez funkcję heurystyczną
        int retScore() { return score; }
        // Zwraca wektor białych pionów
        std::vector<Pawn*> retWhitePawns() { return white; }
        // Zwraca wektor czarncyh pionów 
        std::vector<Pawn*> retBlackPawns() { return black; }
        // Szuka pionka o danych indeksach
        Pawn* findPawn(std::pair<unsigned int, unsigned int> indices);
        // Aktualizuje planszę dając jej pionek 
        void updateBoard(Pawn* pawn, std::pair<unsigned int, unsigned int> new_indices);
        // Sprawdza czy ruch użytkownika jest dopuszczalny, sprawdzenie zasad warcab
        bool checkUserMove(Pawn* pawn, std::pair<unsigned int, unsigned int> new_indices);
        // Funkcja heurystyczna oceniająca planszę 
        void rateBoard(std::string team);
        // Funkcja sprawdzająca czy dany pionek ma możliwość bicia
        unsigned int possibleCapture(Pawn* pawn, Areas & captures, std::pair<int, int> last);
        // Funkcja rekurencyjna badająca WSZYSTKIE możliwe bicia wielokrotne w dowolne strony
        unsigned int searchAllCaptures(Pawn* pawn, std::pair<int, int> last, Areas & captures);
        // Funkcja realizująca bicie
        std::vector<Board*> makeCaptures(Pawn* pawn, std::pair<int, int> last, Areas & captures); 
        // Zwraca wektor plansz ze wszystkimi możliwymi ruchami
        std::vector<Board*> makeMove(std::string team);
        // Funkcja pomocnicza do tworzenia kopii planszy
        void copyPawns(Board* old_board);
        // Usuwa pionek z planszy
        void deletePawn(std::pair<unsigned int, unsigned int> indices);

    private:
        sf::Texture image;                   // Plik z obrazkiem planszy
        std::vector<Pawn*> white;            // Wektor wskaźników na pionki białe
        std::vector<Pawn*> black;            // Wektor wskaźników na pionki czarne
        Pawn* board[board_size][board_size]; // Plansza o stałym rozmiarze
        int score;                           // Ocena planszy zwracana przez funkcję heurystyczną
};

#endif // BOARD_HPP
