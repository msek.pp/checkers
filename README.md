# CHECKERS

Fourth project created as a part of the course "Basic Algorithms and Methods of Artificial Intelligence".
Because of complexity of this project, it is moved into another repository.

## Task

Implement checkers using min-max algorithm to "play with computer" or observe how computer play with itself.
To do window applications used SFML library.

## Status

Checkers display on a screen. User can move pawns. Program checks user's moves and in case of forbidden moves, it doesn't allow to do that. Min-max tree and prediction is implemented, but no optimized, so thats why it is currently turned off. 

## Building project

mkdir build

cd build

cmake ..

make
