#include "game.hpp"

int main()
{
    Game game("Checkers");
    game.run();

    return 0;
}