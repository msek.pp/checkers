#include "../include/board.hpp"

Board::Board() 
{
    image.loadFromFile(BOARDF); // Wczytanie obrazu z pliku
    setTexture(image);
    setPosition(0, 0);
    setTextureRect(sf::IntRect(0, 0, 1040, 1040));
    //setScale(0.80, 0.80);
    score = 0; // Początkowa wartość planszy

    // Stworzenie pionków białych (pionki gracza)
    for(int i = 0; i < pawns; ++i)
    {
        Pawn* new_pawn = new Pawn(WHITETEAM, WHITEP);
        white.push_back(new_pawn);
    }
    // Stworzenie pionków czarnych (pionki AI)
    for(int i = 0; i < pawns; ++i)
    {
        Pawn* new_pawn = new Pawn(BLACKTEAM, BLACKP);
        black.push_back(new_pawn);
    }
    // ======================= Rozmieszczenie pionków AI na planszy ====
    unsigned int num_of_pawn = 0; 
    for(int i = 0; i < 3; ++i)
    {
        for(int j = 0; j < board_size; ++j)
        {
            // Pierwszy i trzeci rząd planszy od góry
            if(i % 2 == 0)
            {
                if(j % 2 == 0)
                {
                    black[num_of_pawn]->setIndex(i, j);
                    black[num_of_pawn]->calcIndices();
                    board[i][j] = black[num_of_pawn];
                    ++num_of_pawn;
                }
                else 
                    board[i][j] = nullptr;
            }
            // Drugi rząd planszy od góry
            if(i % 2 != 0)
            {
                if(j % 2 != 0)
                {
                    black[num_of_pawn]->setIndex(i, j);
                    black[num_of_pawn]->calcIndices();
                    board[i][j] = black[num_of_pawn];
                    ++num_of_pawn;
                }
                else
                    board[i][j] = nullptr;
            }
        }
    }
    // ================== Rozmieszczenie pionków gracza na planszy ====
    num_of_pawn = 0; 
    for(int i = 5; i < board_size; ++i)
    {
        for(int j = 0; j < board_size; ++j)
        {
            // Szósty i ósmy rząd planszy od góry
            if(i % 2 != 0)
            {
                if(j % 2 != 0)
                {
                    white[num_of_pawn]->setIndex(i, j);
                    white[num_of_pawn]->calcIndices();
                    board[i][j] = white[num_of_pawn];
                    ++num_of_pawn;
                }
                else 
                    board[i][j] = nullptr;
            }
            // Siódmy rząd planszy od góry
            if(i % 2 == 0)
            {
                if(j % 2 == 0)
                {
                    white[num_of_pawn]->setIndex(i, j);
                    white[num_of_pawn]->calcIndices();
                    board[i][j] = white[num_of_pawn];
                    ++num_of_pawn;
                }
                else
                    board[i][j] = nullptr;
            }
        }
    }
    // Umieszczenie "nullptr" w polu bitwy
    for(int i = 3; i < 5; ++i)
    {
        for(int j = 0; j < board_size; ++j)
            board[i][j] = nullptr;
    }
}

Board::~Board()
{
    for(auto ptr = white.begin(); ptr != white.end(); ++ptr)
        delete *ptr;
    for(auto ptr = black.begin(); ptr != black.end(); ++ptr)
        delete *ptr;
}

void Board::showBoard() 
{
    std::cout << "\nPlansza:\n\n";
    for(int i = 0; i < board_size; ++i)
    {
        for(int j = 0; j < board_size; ++j)
        {
            if(board[i][j] == nullptr)
                std::cout << "  #  ";
            else
                std::cout << "  " << board[i][j]->retTeam() << "  ";
            
            if(j != 0 && j % (board_size - 1) == 0)
                std::cout << "\n\n";
        }
    }
    
}

Pawn* Board::findPawn(std::pair<unsigned int, unsigned int> indices)
{
    Pawn* found = nullptr;
    // Przeszukanie listy białych pionków
    for(auto ptr = white.begin(); ptr != white.end(); ++ptr)
    {
        if(indices.first == (*ptr)->retIndices().first && indices.second == (*ptr)->retIndices().second)
        {
            found = *ptr;
            break;
        }
    }
    // Jeżeli pionek już znaleziony, to zwróć
    if(found != nullptr)
        return found;
    // Przeszukanie listy czarnych pionków
    for(auto ptr = black.begin(); ptr != black.end(); ++ptr)
    {
        if(indices == (*ptr)->retIndices())
        {
            found = *ptr;
            break;
        }
    }
    return found;
}

void Board::updateBoard(Pawn* pawn, std::pair<unsigned int, unsigned int> new_indices)
{
    // Sprawdzenie czy przeniesiony pionek może zostać damką
    
    // ======== Dodanie pionka na nowe miejsce na planszy ====
    std::pair<unsigned int, unsigned int> old_indices = pawn->retIndices();
    // Wstawienie "nullptr" w stare miejsce
    board[old_indices.first][old_indices.second] = nullptr;
    // Sprawdzenie czy przeniesiony pionek może zostać damką
    if(pawn->retTeam() == "W" && new_indices.first == 0)
        pawn->coronation(WHITEQ);
    else if(pawn->retTeam() == "B" && new_indices.second == (board_size - 1))
        pawn->coronation(BLACKQ);
    // Wstawienie pionka w nowe miejsce
    board[new_indices.first][new_indices.second] = pawn;
    // Aktualizacja indeksów pionka i obliczenie koordynatów na ekranie
    pawn->setIndex(new_indices.first, new_indices.second);
    pawn->calcIndices();
}

bool Board::checkUserMove(Pawn* pawn, std::pair<unsigned int, unsigned int> new_indices)
{
    bool is_legal = true;
    std::pair<unsigned int, unsigned int> pawn_indices = pawn->retIndices();
    // Zmienne mówiące o tym czy pionek jest na krawędzi planszy
    bool on_left_border = (pawn_indices.second == 0) ? true : false;
    bool on_right_border = (pawn_indices.second == board_size - 1) ? true : false;

    // Najpierw sprawdzenie czy wokół pionka są przeciwnicy, jeżeli tak, to sprawdzenie
    // czy użytkownik chce zbić, przejść o pole za dużo czy wejść na zajete pole (niedopuszczalne)

    // Jeżeli pionek jest zwykłym pionkiem (nie damką)
    if(!pawn->isQueen())
    {
        // Jeżeli na polu, które wybrał użytkownik jest pionek 
        if(board[new_indices.first][new_indices.second] != nullptr)
            is_legal =  false;
        else if(on_left_border)
        {
            // Pionek nie może się cofać, iść w bok ani w lini prostej
            if(new_indices.first >= pawn_indices.first || new_indices.second <= pawn_indices.second)
                is_legal = false;
            // Pionek może pójść tylko o jedno pole bezpośrednio po przekątnej w prawo
            else if((pawn_indices.first - new_indices.first > 1) || (new_indices.second - pawn_indices.second > 1))
                is_legal = false;
        }
        else if(on_right_border)
        {
            // Pionek nie może się cofać, iść w bok ani w lini prostej
            if(new_indices.first >= pawn_indices.first || new_indices.second >= pawn_indices.second)
                is_legal = false;
            // Pionek może pójść tylko o jedno pole bezpośrednio po przekątnej w lewo
            else if((pawn_indices.first - new_indices.first > 1) || (pawn_indices.second - new_indices.second > 1))
                is_legal = false;
        }
        else
        {
            // Pionek nie może się cofać, iść w bok ani w lini prostej
            if(new_indices.first >= pawn_indices.first || new_indices.second == pawn_indices.second)
                is_legal = false;
            // Sprawdzenie w którą stronę ma iść pionek
            bool left = (pawn_indices.second > new_indices.second) ? true : false;
            bool right = (pawn_indices.second < new_indices.second) ? true : false;
            // Pionek może pójść tylko o jedno pole bezpośrednio po przekątnej w prawo ...
            if(right)
            {
                if((pawn_indices.first - new_indices.first > 1) || (new_indices.second - pawn_indices.second > 1))
                    is_legal = false;
            }
            // ... albo pionek może pójść tylko o jedno pole bezpośrednio po przekątnej w lewo
            else if(left)
            {
                if((pawn_indices.first - new_indices.first > 1) || (pawn_indices.second - new_indices.second) > 1)
                    is_legal = false;
            }
        }
    }
    // Jeżeli pionek jest damką
    else
    {

    }
    return is_legal;
}

// =============================================== Funkcja heurystyczna ====== //
// ~~~~~~~~~~~~~~ TABLICA PUNKTÓW ~~~~~~~~~~~~~ //
/*
    Pionek na planszy - 5 pkt
    Damka na planszy - 50 pkt
    Możliwość bicia pionka - 15 pkt 
    Pionek na brzegu planszy (bezpieczniejszy) - 2 pkt
    Pionek w rzędzie drugim - 1 pkt
    Pionek w rzędzie trzecim - 2 pkt
    Pionek w rzędzie czwartym - 3 pkt (żeby zachęcać AI do posuwania się do przodu i konfrontacji)
    Wygrana - 1000 pkt
    Przegrana - (-1000) pkt
*/
void Board::rateBoard(std::string team)
{
    score = 0;
    // Ocena planszy na podstawie białej drużyny
    if(team == "W")
    {
        if(black.size() == 0) // Wygrana
            score += 1000;
        if(white.size() == 0) // Przegrana
            score -= 1000;
        for(auto ptr = white.begin(); ptr != white.end(); ++ptr)
        {
            if((*ptr)->isQueen())
                score += 50;
            else
                score += 5;
            std::pair<unsigned int, unsigned int> pawn_indices = (*ptr)->retIndices();
            // Przyznawanie punktów na podstawie położenia
            if(pawn_indices.second == 0 || pawn_indices.second == (board_size - 1))
                score += 2;
            if(pawn_indices.first == 6)
                score += 1;
            else if(pawn_indices.first == 5)
                score += 2;
            else if(pawn_indices.first == 4)
                score += 3;
            
            // ====== SPRAWDZENIE WSZYSTKICH MOŻLIWYCH BIĆ PIONKA (W TYM WIELOKROTNE) ===== //
            Areas all_captures; // Wektor pól na które może przeskoczyć pionek podczas bicia
            Pawn work_pawn = **ptr; // Robocza kopia pionka, żeby nie zmieniać indeksów w oryginale
            unsigned int possible_cap; // Zmienna mówiąca o ilości możliwych bić
            std::pair<int, int> zero_position(-1, -1); // Indeksy inicjalizujące poszukiwanie wszystkich "gałęzi" bić
            possible_cap = searchAllCaptures(&work_pawn, zero_position, all_captures);
            // Dodanie wszystkich możliwych bić do ogólnego wyniku planszy
            score = score + (possible_cap * 15);
            // ============================================================================ //
        }
    }
    // Ocena planszy na podstawie czarnej drużyny
    else if(team == "B")
    {
        if(white.size() == 0) // Wygrana
            score += 1000;
        if(black.size() == 0) // Przegrana
            score -= 1000;
        for(auto ptr = black.begin(); ptr != black.end(); ++ptr)
        {
            if((*ptr)->isQueen())
                score += 50;
            else
                score += 5;
            std::pair<unsigned int, unsigned int> pawn_indices = (*ptr)->retIndices();
            // Przyznawanie punktów na podstawie położenia
            if(pawn_indices.second == 0 || pawn_indices.second == (board_size - 1))
                score += 2;
            if(pawn_indices.first == 1)
                score += 1;
            else if(pawn_indices.first == 2)
                score += 2;
            else if(pawn_indices.first == 3)
                score += 3;
            
            // ====== SPRAWDZENIE WSZYSTKICH MOŻLIWYCH BIĆ PIONKA (W TYM WIELOKROTNE) ===== //
            Areas all_captures; // Wektor pól na które może przeskoczyć pionek podczas bicia
            Pawn work_pawn = **ptr; // Robocza kopia pionka, żeby nie zmieniać indeksów w oryginale
            unsigned int possible_cap; // Zmienna mówiąca o ilości możliwych bić
            std::pair<int, int> zero_position(-1, -1); // Indeksy inicjalizujące poszukiwanie wszystkich "gałęzi" bić
            possible_cap = searchAllCaptures(&work_pawn, zero_position, all_captures);
            // Dodanie wszystkich możliwych bić do ogólnego wyniku planszy
            score = score + (possible_cap * 15);
            // ============================================================================ //
        }
    }
}

// Funkcja oceniająca czy pionek może bić
unsigned int Board::possibleCapture(Pawn* pawn, Areas & captures, std::pair<int, int> last)
{
    // Drużyna pionka
    std::string pawn_team = pawn->retTeam();
    // Indeksy pionka
    std::pair<unsigned int, unsigned int> pawn_indices = pawn->retIndices();
    // Zmienna zliczająca możliwości bić
    unsigned int capture_counter = 0; 

    // Tworzenie indeksów pól sąsiednich skrajnych względem pionka w polu 5x5
    std::pair<int, int> left_up(pawn_indices.first - 2, pawn_indices.second - 2);
    std::pair<int, int> left_down(pawn_indices.first + 2, pawn_indices.second - 2);
    std::pair<int, int> right_up(pawn_indices.first - 2, pawn_indices.second + 2);
    std::pair<int, int> right_down(pawn_indices.first + 2, pawn_indices.second + 2);

    // Sprawdzenie czy dane pole istnieje i czy jest na planszy
    if(left_up.first >= 0 && left_up.first <= (board_size - 1) && left_up.second >= 0 && left_up.second <= (board_size - 1))
    {
        // Sprawdzenie czy skrajne pole jest puste, jeżeli tak to pole niżej po przekątnej
        if(board[left_up.first][left_up.second] == nullptr && (left_up.first != last.first) && (left_up.second != last.second))
        {
            // Jeżeli jest wrogi pionek, to nalicz możliwość bicia
            Pawn* enemy = board[left_up.first + 1][left_up.second + 1];
            if(enemy != nullptr)
            {
                if(enemy->retTeam() == "B" && pawn_team == "W")
                {
                    ++capture_counter;
                    captures.push_back(left_up);
                }
                else if(enemy->retTeam() == "W" && pawn_team == "B")
                {
                    ++capture_counter;
                    captures.push_back(left_up);
                }
            }
        } 
    }
    if(left_down.first >= 0 && left_down.first <= (board_size - 1) && left_down.second >= 0 && left_down.second <= (board_size - 1))
    {
        if(board[left_down.first][left_down.second] == nullptr  && (left_down.first != last.first) && (left_down.second != last.second))
        {
            Pawn* enemy = board[left_down.first - 1][left_down.second + 1];
            if(enemy != nullptr)
            {
                if(enemy->retTeam() == "B" && pawn_team == "W")
                {
                    ++capture_counter;
                    captures.push_back(left_down);
                }
                else if(enemy->retTeam() == "W" && pawn_team == "B")
                {
                    ++capture_counter;
                    captures.push_back(left_down);
                }
            }
        } 
    }
    if(right_up.first >= 0 && right_up.first <= (board_size - 1) && right_up.second >= 0 && right_up.second <= (board_size - 1))
    {
        if(board[right_up.first][right_up.second] == nullptr && (right_up.first != last.first) && (right_up.second != last.second))
        {
            Pawn* enemy = board[right_up.first + 1][right_up.second - 1];
            if(enemy != nullptr)
            {   
                if(enemy->retTeam() == "B" && pawn_team == "W")
                {
                    ++capture_counter;
                    captures.push_back(right_up);
                }
                else if(enemy->retTeam() == "W" && pawn_team == "B")
                {
                    ++capture_counter;
                    captures.push_back(right_up);
                }
            }
        } 
    }
    if(right_down.first >= 0 && right_down.first <= (board_size - 1) && right_down.second >= 0 && right_down.second <= (board_size - 1))
    {
        if(board[right_down.first][right_down.second] == nullptr && (right_down.first != last.first) && (right_down.second != last.second))
        {
            Pawn* enemy = board[right_down.first - 1][right_down.second - 1];
            if(enemy != nullptr)
            {
                if(enemy->retTeam() == "B" && pawn_team == "W")
                {
                    ++capture_counter;
                    captures.push_back(right_down);
                }
                else if(enemy->retTeam() == "W" && pawn_team == "B")
                {
                    ++capture_counter;
                    captures.push_back(right_down);
                }
            }
        } 
    }
    return capture_counter;
}

unsigned int Board::searchAllCaptures(Pawn* pawn, std::pair<int, int> last, Areas & captures)
{
    Areas all_captures; // Wektor pól na które może przeskoczyć pionek podczas bicia
    Pawn work_pawn = *pawn; // Robocza kopia pionka, żeby nie zmieniać indeksów w oryginale
    unsigned int possible_cap; // Zmienna mówiąca o ilości możliwych bić
    unsigned int current_captures = 0;
    // Wywołanie szukania pojedynczych bić w dowolnym kierunku
    possible_cap = possibleCapture(&work_pawn, all_captures, last);
    current_captures += possible_cap;
    // Rekurencyjne wywołanie dla pozostałych pól
    for(auto ptr = all_captures.begin(); ptr != all_captures.end(); ++ptr)
    {
        captures.push_back(*ptr);
        std::pair<unsigned int, unsigned int> last_position = work_pawn.retIndices();
        work_pawn.setIndex((*ptr).first, (*ptr).second);
        current_captures += searchAllCaptures(&work_pawn, last_position, captures);
    }
    return current_captures;
}

// ====================================================================================================================== //
std::vector<Board*> Board::makeMove(std::string team)
{
    std::vector<Board*> made_boards;
    if(team == "W")
    {
        for(auto ptr = white.begin(); ptr != white.end(); ++ptr)
        {
            std::pair<unsigned int, unsigned int> indices = (*ptr)->retIndices();
            // Sprawdzenie możliwości ruchu w lewo pionka
            std::pair<int, int> left_move(indices.first - 1, indices.second - 1);
            if(left_move.first >= 0 && left_move.first <= (board_size - 1) && left_move.second >= 0 && left_move.second <= (board_size - 1))
            {
                if(board[left_move.first][left_move.second] == nullptr)
                {   
                    Board* new_board = new Board; // Nowa plansza
                    new_board->copyPawns(this); // Kopiuje położenia pionków
                    Pawn* new_pawn = new_board->findPawn(indices);
                    new_board->updateBoard(new_pawn, left_move);
                    made_boards.push_back(new_board);
                }
            }
            // Sprawdzenie możliwości ruchu w prawo pionka
            std::pair<int, int> right_move(indices.first - 1, indices.second + 1);
            if(right_move.first >= 0 && right_move.first <= (board_size - 1) && right_move.second >= 0 && right_move.second <= (board_size - 1))
            {
                if(board[right_move.first][right_move.second] == nullptr)
                {   
                    Board* new_board = new Board; // Nowa plansza
                    new_board->copyPawns(this); // Kopiuje położenia pionków
                    Pawn* new_pawn = new_board->findPawn(indices);
                    new_board->updateBoard(new_pawn, right_move);
                    made_boards.push_back(new_board);
                }
            }
            // Sprawdzenie możliwości bicia pionków
            Areas all_captures; // Wektor pól na które może przeskoczyć pionek podczas bicia
            Pawn work_pawn = **ptr; // Robocza kopia pionka, żeby nie zmieniać indeksów w oryginale
            std::pair<int, int> zero_position(-1, -1); // Indeksy inicjalizujące poszukiwanie wszystkich "gałęzi" bić
            std::vector<Board*> capture_boards = makeCaptures(&work_pawn, zero_position, all_captures);;
            // ============================================================================ //
            // Dodaje wszystkie plansze uzyskane z bić / wielokrotnych bić
            if(capture_boards.size() != 0)
                made_boards.insert(made_boards.end(), capture_boards.begin(), capture_boards.end());
        }
    }
    else if(team == "B")
    {
        for(auto ptr = black.begin(); ptr != black.end(); ++ptr)
        {
            std::pair<unsigned int, unsigned int> indices = (*ptr)->retIndices();
            // Sprawdzenie możliwości ruchu w lewo pionka
            std::pair<int, int> left_move(indices.first + 1, indices.second - 1);
            if(left_move.first >= 0 && left_move.first <= (board_size - 1) && left_move.second >= 0 && left_move.second <= (board_size - 1))
            {
                if(board[left_move.first][left_move.second] == nullptr)
                {   
                    Board* new_board = new Board; // Nowa plansza
                    new_board->copyPawns(this); // Kopiuje położenia pionków
                    Pawn* new_pawn = new_board->findPawn(indices);
                    new_board->updateBoard(new_pawn, left_move);
                    made_boards.push_back(new_board);
                }
            }
            // Sprawdzenie możliwości ruchu w prawo pionka
            std::pair<int, int> right_move(indices.first + 1, indices.second + 1);
            if(right_move.first >= 0 && right_move.first <= (board_size - 1) && right_move.second >= 0 && right_move.second <= (board_size - 1))
            {
                if(board[right_move.first][right_move.second] == nullptr)
                {   
                    Board* new_board = new Board; // Nowa plansza
                    new_board->copyPawns(this); // Kopiuje położenia pionków
                    Pawn* new_pawn = new_board->findPawn(indices);
                    new_board->updateBoard(new_pawn, right_move);
                    made_boards.push_back(new_board);
                }
            }
            // Sprawdzenie możliwości bicia pionków
            Areas all_captures; // Wektor pól na które może przeskoczyć pionek podczas bicia
            Pawn work_pawn = **ptr; // Robocza kopia pionka, żeby nie zmieniać indeksów w oryginale bić
            std::pair<int, int> zero_position(-1, -1); // Indeksy inicjalizujące poszukiwanie wszystkich "gałęzi" bić
            std::vector<Board*> capture_boards = makeCaptures(&work_pawn, zero_position, all_captures);
            // ============================================================================ //
            // Dodaje wszystkie plansze uzyskane z bić / wielokrotnych bić
            if(capture_boards.size() != 0)
                made_boards.insert(made_boards.end(), capture_boards.begin(), capture_boards.end());
        }
    }
    return made_boards;
}
// ====================================================================================================================== //



// Przepisuje położenie pionków na podstawie danej planszy
void Board::copyPawns(Board* old_board)
{
    // Wektory indeksów pionków danej planszy
    std::vector<std::pair<unsigned int, unsigned int>> white_pos;
    std::vector<std::pair<unsigned int, unsigned int>> black_pos;
    std::vector<Pawn*> white_old = old_board->retWhitePawns();
    std::vector<Pawn*> black_old = old_board->retBlackPawns();

    for(auto ptr = white_old.begin(); ptr != white_old.end(); ++ptr)
        white_pos.push_back((*ptr)->retIndices());
    for(auto ptr = black_old.begin(); ptr != black_old.end(); ++ptr)
        black_pos.push_back((*ptr)->retIndices());

    int i = 0;
    for(auto ptr = white_pos.begin(); ptr != white_pos.end(); ++ptr)
    {
        updateBoard(white[i], *ptr);
        ++i;
    }
    i = 0;
    for(auto ptr = black_pos.begin(); ptr != black_pos.end(); ++ptr)
    {
        updateBoard(black[i], *ptr);
        ++i;
    }
}

// ==================================================================== // 
std::vector<Board*> Board::makeCaptures(Pawn* pawn, std::pair<int, int> last, Areas & captures)
{
    std::vector<Board*> boards;
    Areas all_captures; // Wektor pól na które może przeskoczyć pionek podczas bicia
    Pawn work_pawn = *pawn; // Robocza kopia pionka, żeby nie zmieniać indeksów w oryginale
    unsigned int possible_cap; // Zmienna mówiąca o ilości możliwych bić
    // Wywołanie szukania pojedynczych bić w dowolnym kierunku
    possible_cap = possibleCapture(&work_pawn, all_captures, last);

    // ========= Jeżeli koniec ścieżki zbijania, to zrób nową planszę ==========
    if(possible_cap == 0 && captures.size() != 0)
    {
        captures.push_back(pawn->retIndices());
        // WYKONAĆ BICIE WIELOKROTNE
        Board* new_board = new Board; // Nowa plansza
        new_board->copyPawns(this); // Kopiuje położenia pionków
        
        for(auto ptr = captures.begin(); ptr != captures.end() - 1; ++ptr)
        {
            // Przesuwanie pionka
            auto next = *(ptr + 1);
            Pawn* new_pawn = new_board->findPawn(*ptr);
            new_board->updateBoard(new_pawn, next);
            // Animacja pionka
            // Usuwanie pionka 
            // Tworzenie indeksów pól sąsiednich skrajnych względem pionka w polu 3x3
            std::pair<int, int> left_up((*ptr).first - 1, (*ptr).second - 1);
            std::pair<int, int> left_down((*ptr).first + 1, (*ptr).second - 1);
            std::pair<int, int> right_up((*ptr).first - 1, (*ptr).second + 1);
            std::pair<int, int> right_down((*ptr).first + 1, (*ptr).second + 1);

            if(left_up.first - 1 == next.first && left_up.second - 1 == next.second)
                new_board->deletePawn(left_up);
            if(left_down.first + 1 == next.first && left_down.second - 1 == next.second)
                new_board->deletePawn(left_down);
            if(right_up.first - 1 == next.first && right_up.second + 1 == next.second)
                new_board->deletePawn(right_up);
            if(right_down.first + 1 == next.first && right_down.second + 1 == next.second)
                new_board->deletePawn(right_down);
        }

        boards.push_back(new_board);
        // =====================================================================
        return boards;
    }

    captures.push_back(pawn->retIndices());

    // Rekurencyjne wywołanie dla pozostałych pól
    for(auto ptr = all_captures.begin(); ptr != all_captures.end(); ++ptr)
    {
        std::vector<Board*> temp;
        std::pair<unsigned int, unsigned int> last_position = work_pawn.retIndices();
        work_pawn.setIndex((*ptr).first, (*ptr).second);
        temp = makeCaptures(&work_pawn, last_position, captures);
        boards.insert(boards.end(), temp.begin(), temp.end());
    }
    return boards;
}

void Board::deletePawn(std::pair<unsigned int, unsigned int> indices)
{
    Pawn* del_pawn = findPawn(indices);
    board[indices.first][indices.second] = nullptr;
    std::vector<Pawn*>::iterator new_end;
    if(del_pawn->retTeam() == "W")
        new_end = std::remove(white.begin(), white.end(), del_pawn);
    else if(del_pawn->retTeam() == "B")
        new_end = std::remove(black.begin(), black.end(), del_pawn);
}