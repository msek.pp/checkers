#include "../include/pawn.hpp"

Pawn::Pawn(std::string color, const std::string filename)
{
    team = color;
    image.loadFromFile(filename);
    setTexture(image);
    setOrigin(60, 60);
    setTextureRect(sf::IntRect(0, 0, 120, 120));
}

void Pawn::calcIndices()
{
    unsigned int index1 = indices.first + 1;
    unsigned int index2 = indices.second + 1;
    coords.first = (index1 * 120) - 20;
    coords.second = (index2 * 120) - 20;
    setPosition(sf::Vector2f(coords.second, coords.first));
}