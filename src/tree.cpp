#include "../include/tree.hpp"

GameTree::GameTree(Board* root, unsigned int max_depth, std::string team)
{
    // Wraca jeżeli doszło do określonej głębokości
    if(max_depth == 0)
        return;
    
    if(team == "W")
    {
        std::vector<Board*> all_possible = root->makeMove("B");
        for(auto ptr = all_possible.begin(); ptr != all_possible.end(); ++ptr)
        {
            GameTree* new_tree = new GameTree((*ptr), max_depth - 1, "B");
            next_boards.push_back(new_tree);
        }
    }
    if(team == "B")
    {
        std::vector<Board*> all_possible = root->makeMove("W");
        for(auto ptr = all_possible.begin(); ptr != all_possible.end(); ++ptr)
        {
            GameTree* new_tree = new GameTree((*ptr), max_depth - 1, "W");
            next_boards.push_back(new_tree);
        }
    }
    std::cout << "\n\nmax_depth = " << max_depth;
}
