#include "../include/game.hpp"

#define MAX_DEPTH 3

// Funkcja pomocnicza licząca indeksy na planszy z pozycji myszki
std::pair<unsigned int, unsigned int> calcFromMouse(int x, int y);

Game::Game(sf::String title) : sf::RenderWindow(sf::VideoMode(SCREENX, SCREENY, DEPTH), title)
{
    setActive(true); // Ustawia, że okno jest aktywne
    setFramerateLimit(30); // Ustawia limit klatek na sekundę
    setPosition(sf::Vector2i(0, 0)); // Ustawia loaklizację okna
    setKeyRepeatEnabled(false);
}

void Game::run()
{
    std::vector<Pawn*> white;
    std::vector<Pawn*> black;
    sf::Event event;
    // Zmienne pomocnicze do obsługi działań gracza 
    int x, y;
    std::pair<unsigned int, unsigned int> indices;
    Pawn* pawn;
    bool pawn_down = false;
    std::pair<int, int> fisrt_coords; // Początkowe położenie pionka na planszy 
    // Pętla główna
    while(isOpen())
    {
        while(pollEvent(event))
        {
            if(event.type == sf::Event::Closed || (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)))
                close();
            
            if(event.type == sf::Event::MouseButtonPressed)
            {
                // Proces szukania pionka na bazie wspołrzędnych wciśniętego kursora
                x = sf::Mouse::getPosition().x;
                y = sf::Mouse::getPosition().y;
                indices = calcFromMouse(y, x);
                pawn = board.findPawn(indices); // Znaleziony pionek!!!
                // Jeżeli pionek znaleziono, to pobierz jego położenie początkowe na ekranie
                if(pawn != nullptr)
                    fisrt_coords = pawn->retCoords();
                std::cout << "\nPodniesienie: x = " << indices.first << ", y = " << indices.second;
            }
            if(event.type == sf::Event::MouseButtonReleased)
                pawn_down = true;
        
            if(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Space)
            {
                board.showBoard();
                board.rateBoard("W");
                //std::cout << "\nOcena planszy względem białych pionów: " << board.retScore();
            }
        }
        // Jeżeli przycisk myszy jest wciśnięty, to przesuwaj pionek
        if(sf::Mouse::isButtonPressed(sf::Mouse::Left) && pawn != nullptr)
        {
            if(pawn->retTeam() == "W")
            {
                pawn->setScale(1.3,1.3);
                pawn->move(sf::Mouse::getPosition().x, sf::Mouse::getPosition().y);
            }
        }
        // Opuszczenie pionka na pole
        if(pawn_down && pawn != nullptr && pawn->retTeam() == "W")
        {
            pawn_down = false;
            pawn->setScale(1, 1);
            x = sf::Mouse::getPosition().x;
            y = sf::Mouse::getPosition().y;
            indices = calcFromMouse(y, x);
            // Sprawdzenie czy ruch użytkownika jest dozwolony
            if(board.checkUserMove(pawn, indices))
                board.updateBoard(pawn, indices);
            else
                pawn->setPosition(fisrt_coords.second, fisrt_coords.first);

            std::cout << "\nOpuszczenie: x = " << indices.first << ", y = " << indices.second;
            //GameTree moves_tree(&board, MAX_DEPTH, "W");
        }

        draw(board);
        
        white = board.retWhitePawns();
        black = board.retBlackPawns();

        for(auto ptr = white.begin(); ptr != white.end(); ++ptr)
            draw(**ptr);
        for(auto ptr = black.begin(); ptr != black.end(); ++ptr)
            draw(**ptr);

        display();
    }
}


std::pair<unsigned int, unsigned int> calcFromMouse(int x, int y)
{
    int l_border = 40;    // Granica lewa i górna wynikająca z ilości pikseli na planszy
    int side = 120;
    std::pair<unsigned int, unsigned int> indices;
    // Tworzenie wektora granic
    std::vector<int> borders;
    for(int i = 1; i < 9; ++i)
        borders.push_back((side * i) + l_border);

    // Pierwsza kolumna
    if(x > l_border && x < borders[0])
    {
        indices.first = 0;   
        if(y > l_border && y < borders[0])
            indices.second = 0;
        if(y > borders[0] && y < borders[1])
            indices.second = 1;
        if(y > borders[1] && y < borders[2])
            indices.second = 2;
        if(y > borders[2] && y < borders[3])
            indices.second = 3;
        if(y > borders[3] && y < borders[4])
            indices.second = 4;
        if(y > borders[4] && y < borders[5])
            indices.second = 5;
        if(y > borders[5] && y < borders[6])
            indices.second = 6;
        if(y > borders[6] && y < borders[7])
            indices.second = 7;
    }
    // Druga kolumna
    if(x > borders[0] && x < borders[1])
    {
        indices.first = 1;   
        if(y > l_border && y < borders[0])
            indices.second = 0;
        if(y > borders[0] && y < borders[1])
            indices.second = 1;
        if(y > borders[1] && y < borders[2])
            indices.second = 2;
        if(y > borders[2] && y < borders[3])
            indices.second = 3;
        if(y > borders[3] && y < borders[4])
            indices.second = 4;
        if(y > borders[4] && y < borders[5])
            indices.second = 5;
        if(y > borders[5] && y < borders[6])
            indices.second = 6;
        if(y > borders[6] && y < borders[7])
            indices.second = 7;
    }
    // Trzecia kolumna
    if(x > borders[1] && x < borders[2])
    {
        indices.first = 2;   
        if(y > l_border && y < borders[0])
            indices.second = 0;
        if(y > borders[0] && y < borders[1])
            indices.second = 1;
        if(y > borders[1] && y < borders[2])
            indices.second = 2;
        if(y > borders[2] && y < borders[3])
            indices.second = 3;
        if(y > borders[3] && y < borders[4])
            indices.second = 4;
        if(y > borders[4] && y < borders[5])
            indices.second = 5;
        if(y > borders[5] && y < borders[6])
            indices.second = 6;
        if(y > borders[6] && y < borders[7])
            indices.second = 7;
    }
    // Czwarta kolumna
    if(x > borders[2] && x < borders[3])
    {
        indices.first = 3;   
        if(y > l_border && y < borders[0])
            indices.second = 0;
        if(y > borders[0] && y < borders[1])
            indices.second = 1;
        if(y > borders[1] && y < borders[2])
            indices.second = 2;
        if(y > borders[2] && y < borders[3])
            indices.second = 3;
        if(y > borders[3] && y < borders[4])
            indices.second = 4;
        if(y > borders[4] && y < borders[5])
            indices.second = 5;
        if(y > borders[5] && y < borders[6])
            indices.second = 6;
        if(y > borders[6] && y < borders[7])
            indices.second = 7;
    }
    // Piąta kolumna
    if(x > borders[3] && x < borders[4])
    {
        indices.first = 4;   
        if(y > l_border && y < borders[0])
            indices.second = 0;
        if(y > borders[0] && y < borders[1])
            indices.second = 1;
        if(y > borders[1] && y < borders[2])
            indices.second = 2;
        if(y > borders[2] && y < borders[3])
            indices.second = 3;
        if(y > borders[3] && y < borders[4])
            indices.second = 4;
        if(y > borders[4] && y < borders[5])
            indices.second = 5;
        if(y > borders[5] && y < borders[6])
            indices.second = 6;
        if(y > borders[6] && y < borders[7])
            indices.second = 7;
    }
    // Szósta kolumna
    if(x > borders[4] && x < borders[5])
    {
        indices.first = 5;   
        if(y > l_border && y < borders[0])
            indices.second = 0;
        if(y > borders[0] && y < borders[1])
            indices.second = 1;
        if(y > borders[1] && y < borders[2])
            indices.second = 2;
        if(y > borders[2] && y < borders[3])
            indices.second = 3;
        if(y > borders[3] && y < borders[4])
            indices.second = 4;
        if(y > borders[4] && y < borders[5])
            indices.second = 5;
        if(y > borders[5] && y < borders[6])
            indices.second = 6;
        if(y > borders[6] && y < borders[7])
            indices.second = 7;
    }
    // Siódma kolumna
    if(x > borders[5] && x < borders[6])
    {
        indices.first = 6;   
        if(y > l_border && y < borders[0])
            indices.second = 0;
        if(y > borders[0] && y < borders[1])
            indices.second = 1;
        if(y > borders[1] && y < borders[2])
            indices.second = 2;
        if(y > borders[2] && y < borders[3])
            indices.second = 3;
        if(y > borders[3] && y < borders[4])
            indices.second = 4;
        if(y > borders[4] && y < borders[5])
            indices.second = 5;
        if(y > borders[5] && y < borders[6])
            indices.second = 6;
        if(y > borders[6] && y < borders[7])
            indices.second = 7;
    }
    // Ósma kolumna
    if(x > borders[6] && x < borders[7])
    {
        indices.first = 7;   
        if(y > l_border && y < borders[0])
            indices.second = 0;
        if(y > borders[0] && y < borders[1])
            indices.second = 1;
        if(y > borders[1] && y < borders[2])
            indices.second = 2;
        if(y > borders[2] && y < borders[3])
            indices.second = 3;
        if(y > borders[3] && y < borders[4])
            indices.second = 4;
        if(y > borders[4] && y < borders[5])
            indices.second = 5;
        if(y > borders[5] && y < borders[6])
            indices.second = 6;
        if(y > borders[6] && y < borders[7])
            indices.second = 7;
    }
    return indices;
}